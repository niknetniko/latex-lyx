package be.almiro.java.converter;

/**
 * @author Niko Strijbol
 */
public class NoteConverter {

    public static void main(String[] args) throws Exception {

        String filename;
        if(args.length != 1) {
            filename = "best3";
            System.out.println("Warning! No filename given, trying debug filename.");
        } else {
            filename = args[0];
        }

        if(filename.endsWith(".ppt")) {
            filename = filename.substring(0, -4);
        }

        PowerPointFile file = new PowerPointFile(filename);
        LyxWriter writer = new LyxWriter(filename);
        file.setWriter(writer);
        file.write();

        writer.close();
    }
}