package be.almiro.java.converter;

import org.apache.poi.hslf.usermodel.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Niko Strijbol
 */
public class PowerPointFile {

    private final String name;
    private final HSLFSlideShow slide;
    private Writer writer;

    public PowerPointFile(String file) throws IOException {
        name = file;
        if(!file.endsWith(".ppt")) {
            file = file + ".ppt";
        }
        this.slide = new HSLFSlideShow(new HSLFSlideShowImpl(file));
    }

    public void setWriter(Writer writer) {
        this.writer = writer;
    }

    public void write() throws IOException {

        writer.beginDocument();

        List<HSLFSlide> slides = slide.getSlides();

        for(HSLFSlide oneSlide: slides) {

            writer.startSlide(oneSlide.getSlideNumber());

            HSLFNotes notes = oneSlide.getNotes();

            if(notes != null) {
                for(List<HSLFTextParagraph> paragraphs: oneSlide.getNotes().getTextParagraphs()) {
                    writer.write(handleParagraphs(paragraphs));
                }
            }

            writer.endSlide(oneSlide.getSlideNumber());
        }

        writer.endDocument();
    }

    private List<String> handleParagraphs(List<HSLFTextParagraph> paragraphs) {
        List<String> result = new ArrayList<>();
        for(HSLFTextParagraph paragraph: paragraphs) {
            if(paragraph.isBullet()) {
                paragraph.getBulletChar();
                result.add(writer.writeBulletList(handleTextRuns(paragraph.getTextRuns())));
            } else {
                result.add(String.join("\n", handleTextRuns(paragraph.getTextRuns())));
            }
        }

        return result;
    }

    private List<String> handleTextRuns(List<HSLFTextRun> textRuns) {
        List<String> results = new ArrayList<>();
        for(HSLFTextRun textRun : textRuns) {
            String text = textRun.getRawText();

            if(textRun.isBold()) {
                text = writer.writeBold(text);
            }

            if(textRun.isUnderlined()) {
                text = writer.writeUnderlined(text);
            }

            if(textRun.isSubscript()) {
                text = writer.writeSubScript(text);
            }

            if(textRun.isSuperscript()) {
                text = writer.writeSuperScript(text);
            }

            results.add(text);
        }

        return results;
    }
}