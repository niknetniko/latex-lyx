package be.almiro.java.converter;

import java.io.IOException;

/**
 * @author Niko Strijbol
 */
public interface Writer {

    void write(Iterable<String> text) throws IOException;

    void startSlide(int slide) throws IOException;
    void endSlide(int slide);


    String writeSubScript(String subscript);
    String writeSuperScript(String superscript);
    String writeBulletList(Iterable<String> items);
    String writeUnderlined(String string);
    String writeBold(String string);

    default void beginDocument() {}

    default void endDocument() {}
}