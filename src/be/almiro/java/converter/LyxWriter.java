package be.almiro.java.converter;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author Niko Strijbol
 */
public class LyxWriter implements Writer, AutoCloseable {

    private final PrintWriter writer;
    private final String name;

    public LyxWriter(String filename) throws FileNotFoundException {
        writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename + ".lyx"), StandardCharsets.UTF_8));
        name = filename;
    }

    @Override
    public void write(Iterable<String> paragraphs) throws IOException {
        for(String paragraph: paragraphs) {
            //If it is the weird stuff, stop it now.
            if(paragraph.startsWith("best")) {
                return;
            }
            //If there is no text, skip it.
            if(paragraph.isEmpty()) {
                continue;
            }

            writer.println("\\begin_layout Standard\n" + paragraph + "\n\\end_layout");
        }
    }

    private void writeImage(String imageName) throws IOException {
        writer.println("\\begin_layout Standard\n" +
                "\\noindent\n" +
                "\\align center\n" +
                "\\begin_inset ERT\n" +
                "status open\n" +
                "\n" +
                "\\begin_layout Plain Layout\n" +
                "\n" +
                "\\backslash\n" +
                "frame{\n" +
                "\\backslash\n" +
                "includegraphics[width=0.9\n" +
                "\\backslash\n" +
                "columnwidth]{" + imageName + "}}\n" +
                "\\end_layout\n" +
                "\n" +
                "\\end_inset\n" +
                "\n" +
                "\\end_layout");
    }

    @Override
    public void startSlide(int slide) throws IOException {
        //Write image stuff
        writeImage(name + "-" + slide);
    }

    @Override
    public void endSlide(int slide) {
        writer.println("\\begin_layout Standard\n" +
                "\\begin_inset Newpage newpage\n" +
                "\\end_inset\n" +
                "\\end_layout");
    }

    @Override
    public String writeSubScript(String subscript) {
        return "\\begin_inset script subscript\n" +
                "\\begin_layout Plain Layout\n" +
                subscript + "\n" +
                "\\end_layout\n" +
                "\\end_inset";
    }

    @Override
    public String writeSuperScript(String superscript) {
        return "\\begin_inset script superscript\n" +
                "\\begin_layout Plain Layout\n" +
                superscript + "\n" +
                "\\end_layout\n" +
                "\\end_inset";
    }

    @Override
    public String writeBulletList(Iterable<String> items) {
        String string = "";
        for(String item: items) {
            string += "\\begin_layout Itemize\n" + item + "\\end_layout";
        }
        return string;
    }

    @Override
    public String writeUnderlined(String string) {
        return "\\bar under\n" + string + "\n\\bar default";
    }

    @Override
    public String writeBold(String string) {
        return "\\series bold\n" + string + "\n\\series default";
    }

    @Override
    public void close() throws Exception {
        writer.close();
    }

    public void beginDocument() {
        writer.println("#Niko's BBC (Big Black Creator) created this file. For more info see http://www.lyx.org/\n" +
                "\\lyxformat 508\n" +
                "\\begin_document\n" +
                "\\begin_header\n" +
                "\\textclass article\n" +
                "\\begin_preamble\n" +
                "\\usepackage{graphicx}\n" +
                "\n" +
                "\\newcommand{\\fakesection}[1]{%\n" +
                "  \\par\\refstepcounter{section}% Increase section counter\n" +
                "  \\sectionmark{#1}% Add section mark (header)\n" +
                "  \\addcontentsline{toc}{section}{\\protect\\numberline{\\thesection}#1}% Add section to ToC\n" +
                "  % Add more content here, if needed.\n" +
                "}\n" +
                "\n" +
                "\\newcommand{\\fakesubsection}[1]{%\n" +
                "  \\par\\refstepcounter{subsection}% Increase subsection counter\n" +
                "  \\subsectionmark{#1}% Add subsection mark (header)\n" +
                "  \\addcontentsline{toc}{subsection}{\\protect\\numberline{\\thesubsection}#1}% Add subsection to ToC\n" +
                "  % Add more content here, if needed.\n" +
                "}\n" +
                "\n" +
                "\\usepackage{type1cm}\n" +
                "\\renewcommand\\normalsize{%\n" +
                "   \\@setfontsize\\normalsize{13pt}{14.5pt}%\n" +
                "   \\abovedisplayskip 12\\p@ \\@plus3\\p@ \\@minus7\\p@\n" +
                "   \\abovedisplayshortskip \\z@ \\@plus3\\p@\n" +
                "   \\belowdisplayshortskip 6.5\\p@ \\@plus3.5\\p@ \\@minus3\\p@\n" +
                "   \\belowdisplayskip \\abovedisplayskip\n" +
                "   \\let\\@listi\\@listI}\\normalsize  \n" +
                "\\end_preamble\n" +
                "\\use_default_options true\n" +
                "\\maintain_unincluded_children false\n" +
                "\\language dutch\n" +
                "\\language_package default\n" +
                "\\inputencoding auto\n" +
                "\\fontencoding global\n" +
                "\\font_roman \"lmodern\" \"default\"\n" +
                "\\font_sans \"lmss\" \"default\"\n" +
                "\\font_typewriter \"lmtt\" \"default\"\n" +
                "\\font_math \"auto\" \"auto\"\n" +
                "\\font_default_family default\n" +
                "\\use_non_tex_fonts false\n" +
                "\\font_sc false\n" +
                "\\font_osf false\n" +
                "\\font_sf_scale 100 100\n" +
                "\\font_tt_scale 100 100\n" +
                "\\graphics default\n" +
                "\\default_output_format default\n" +
                "\\output_sync 0\n" +
                "\\bibtex_command default\n" +
                "\\index_command default\n" +
                "\\paperfontsize 12\n" +
                "\\spacing single\n" +
                "\\use_hyperref true\n" +
                "\\pdf_bookmarks true\n" +
                "\\pdf_bookmarksnumbered false\n" +
                "\\pdf_bookmarksopen false\n" +
                "\\pdf_bookmarksopenlevel 1\n" +
                "\\pdf_breaklinks false\n" +
                "\\pdf_pdfborder true\n" +
                "\\pdf_colorlinks false\n" +
                "\\pdf_backref false\n" +
                "\\pdf_pdfusetitle true\n" +
                "\\papersize a4paper\n" +
                "\\use_geometry true\n" +
                "\\use_package amsmath 1\n" +
                "\\use_package amssymb 1\n" +
                "\\use_package cancel 1\n" +
                "\\use_package esint 1\n" +
                "\\use_package mathdots 1\n" +
                "\\use_package mathtools 1\n" +
                "\\use_package mhchem 1\n" +
                "\\use_package stackrel 1\n" +
                "\\use_package stmaryrd 1\n" +
                "\\use_package undertilde 1\n" +
                "\\cite_engine basic\n" +
                "\\cite_engine_type default\n" +
                "\\biblio_style plain\n" +
                "\\use_bibtopic false\n" +
                "\\use_indices false\n" +
                "\\paperorientation portrait\n" +
                "\\suppress_date false\n" +
                "\\justification true\n" +
                "\\use_refstyle 1\n" +
                "\\index Index\n" +
                "\\shortcut idx\n" +
                "\\color #008000\n" +
                "\\end_index\n" +
                "\\leftmargin 2cm\n" +
                "\\topmargin 2cm\n" +
                "\\rightmargin 2cm\n" +
                "\\bottommargin 2cm\n" +
                "\\secnumdepth 3\n" +
                "\\tocdepth 3\n" +
                "\\paragraph_separation skip\n" +
                "\\defskip medskip\n" +
                "\\quotes_language english\n" +
                "\\papercolumns 1\n" +
                "\\papersides 1\n" +
                "\\paperpagestyle default\n" +
                "\\tracking_changes false\n" +
                "\\output_changes false\n" +
                "\\html_math_output 0\n" +
                "\\html_css_as_file 0\n" +
                "\\html_be_strict false\n" +
                "\\end_header\n" +
                "\n" +
                "\\begin_body");
    }

    @Override
    public void endDocument() {
        writer.println("\\end_body\n\\end_document");
    }
}
