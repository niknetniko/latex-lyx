package be.almiro.java.converter.old;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Niko Strijbol
 */
public class TexFile {

    private final String content;

    public TexFile(Path path) throws IOException {
        content = new String(Files.readAllBytes(path));
    }

    public Matcher search(Pattern pattern) {
        return pattern.matcher(content);
    }
}