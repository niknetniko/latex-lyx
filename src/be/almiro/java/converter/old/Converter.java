package be.almiro.java.converter.old;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Niko Strijbol
 */
public class Converter {

    private final TexFile texFile;
    private final LyxFile lyxFile;

    public Converter(String filename) throws IOException {
        lyxFile = new LyxFile(filename + ".lyx");
        texFile = new TexFile(Paths.get(filename + ".tex"));
    }

    public void convert() throws IOException {
        Pattern pattern = Pattern.compile(
                "\\\\begin\\{center\\}\\R\\\\frame\\{\\\\includegraphics\\[width=0\\.9\\\\columnwidth\\]\\{([A-Za-z0-9-]+.png)\\}\\}\\R\\\\end\\{center\\}\\R(.*?)(?=\\\\newpage\\{\\})",
                Pattern.DOTALL
        );

        Matcher matcher = texFile.search(pattern);

        lyxFile.writeStart();
        while(matcher.find()) {
            lyxFile.writeImage(matcher.group(1));
            lyxFile.writeText(matcher.group(2));
            lyxFile.writeNewPage();
        }
        lyxFile.writeEnd();

        lyxFile.close();
    }
}