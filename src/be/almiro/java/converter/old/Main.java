package be.almiro.java.converter.old;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        String name;
        if(args.length != 1) {
            name = "best1";
        } else {
            name = args[0];
        }

        final Converter converter = new Converter(name);

        converter.convert();

        System.out.println("Done. Have fun.");
    }
}