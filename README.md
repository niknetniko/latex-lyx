# Powerpoint converter #

The converter is not perfect, so please proofread the result.

### Use ###
See `NoteConverter.java`.

Note: due to limitations with the Java library used to parse PowerPoint files, this will not generate images of your PowerPoint. You should do that yourself. This can easily be done: https://support.microsoft.com/en-us/kb/827745.

Currently the images should be named `[SLIDESHOW NAME]-[SLIDE NUMBER]`. This is hardcoded at the moment, but only in the `LyxWriter`. If you implement your own, you can do what you want.

### Convert to other formats ###

1. Implement `Writer.java` interface.
2. Change `NoteConverter.java` to support your Writer.
3. Done.

See the `LyxWriter` for an example.